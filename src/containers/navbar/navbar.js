import React from 'react';
import './navbar.scss';
import RaisedButton from 'material-ui/RaisedButton';
import { NavLink, Link } from 'react-router-dom';
import { connect } from 'react-redux';
import {LOGIN_USER} from "../../redux/actionConst";
import {fakeAuth} from "../../shared/authenticate";

class Navbar extends React.Component {
    constructor(props) {
        super(props)
    }

    componentWillMount() {
        if (localStorage.getItem("authorization")) {
            this.props.successLogin(true);
            fakeAuth.authenticate(true);
        } else {
            this.props.successLogin(false);
            fakeAuth.signout(true);
        }
    }

    handleLogOut = () => {
      localStorage.removeItem('authorization');
      fakeAuth.signout(true);
      this.props.history.push('/');
      this.props.successLogin(false);
    };

    render() {
        return (
            <header className='header'>
                <div>
                    <NavLink className="header--link" activeClassName="header--active-link" exact to="/">Home</NavLink>
                    <NavLink className="header--link" activeClassName="header--active-link" to="/user">Users</NavLink>
                </div>
                {
                    !this.props.login
                    ? <Link to="/login">
                            <RaisedButton
                                label="Sign in"
                                primary={true}
                            />
                        </Link>
                    : <RaisedButton
                            label="Sign out"
                            primary={true}
                            onClick={this.handleLogOut}
                        />
                }
            </header>
        )
    }
}

export default connect(
    state => ({
        login: state.userLogin
    }),
    dispatch => ({
        successLogin: (el) => dispatch ({type: LOGIN_USER, payload: el }),
    })
    )(Navbar)