import React from 'react';
import TextField from 'material-ui/TextField';
import { User } from '../../components/users/users';
import './userList.scss';
import { searchInput } from '../../styles/materialStyles/userStyles';
import { userList } from "../../action/userInfo";

export default  class UserList extends React.Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
    }

    state = {
        searchValue: '',
        users: [],
    };

   async handleChange (e) {
        this.setState({searchValue: e.target.value});
        try {
            if (e.target.value.length) {
              const result = await userList(e.target.value);
              this.setState({users: result.data.items});
            } else {
                this.setState({users: []});
            }
        } catch (error) {
            console.log(error)
        }
    };

    render() {
       return (
            <div className="user-list">
                <TextField
                    value={this.state.searchValue}
                    hintText="Search user"
                    floatingLabelText="Search user"
                    onChange={this.handleChange}
                    style={searchInput}
                />
                    {
                        this.state.users.map( el =>
                            <User
                                key={el.id}
                                name={el.login}
                                linkGit={el.html_url}
                            />
                        )
                    }
            </div>
        )
    }
}
