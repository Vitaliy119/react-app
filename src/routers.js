import React, { Fragment } from 'react';
import {Switch, Route, Redirect } from 'react-router-dom';
import Navbar from './containers/navbar/navbar';
import UserList from './containers/userList/userList';
import HomePage from './containers/homePage/homePage';
import Login from './components/login/login';
import CurrentUser from './containers/currentUser/currentUser';
import { fakeAuth } from "./shared/authenticate";

const PrivateRoute = ({ component: Component, ...rest }) => (
    <Route
        {...rest}
        render={props =>
            fakeAuth.isAuthenticated ? (
                <Component {...props} />
            ) : (
                <Redirect
                    to={{
                        pathname: "/",
                        state: { from: props.location }
                    }}
                />
            )
        }
    />
);

export const Routers = () => (
    <Fragment>
        <Switch>
            <Route exact path="/login" component={ Login } />
            <Route path="*" component={ Navbar } />
        </Switch>
        <Route exact path="/" component={HomePage} />
        <PrivateRoute exact path="/user" component={UserList} />
        <PrivateRoute exact path="/user/:id" component={CurrentUser} />
    </Fragment>
);

