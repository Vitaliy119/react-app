import React from 'react';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import { connect } from 'react-redux';
import { LOGIN_USER } from "../../redux/actionConst";
import { fakeAuth } from "../../shared/authenticate";
import "./login.scss";

class Login extends React.Component {
    constructor(props) {
        super(props);
    }

    state = {
        login: '',
        password: ''
    };

    handleChange = (e) => {
        this.setState({[e.target.name]: e.target.value})
    };

    handleSave = () => {
        const authorization = this.state.login + this.state.password;
        localStorage.setItem("authorization", authorization);
        fakeAuth.authenticate(true);
        this.props.history.push('/');
        this.props.successLogin(true)
    };

    render() {
        return (
            <div className="login">
                <div className="login__block">
                    <TextField
                        value={this.state.login}
                        name="login"
                        hintText="Login"
                        floatingLabelText="Login"
                        onChange={this.handleChange}
                    />
                    <TextField
                        value={this.state.password}
                        name="password"
                        hintText="Password"
                        type="password"
                        floatingLabelText="Password"
                        onChange={this.handleChange}
                    />
                    <RaisedButton
                        label="Sign In"
                        primary={true}
                        disabled={!this.state.login || !this.state.password}
                        style={{marginTop: '25px'}}
                        onClick={this.handleSave}
                    />
                </div>
            </div>
        )
    }
}

export default connect(null,
    dispatch => ({
        successLogin: (el) => dispatch ({type: LOGIN_USER, payload: el }),
    })
    )(Login)