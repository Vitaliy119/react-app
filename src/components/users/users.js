import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import './users.scss'

export function User (props) {
    return (
        <div className="user">
            <h3 className="user--header">User login: {props.name}</h3>

            <Link className="user--link" to={`/user/${props.name}`}>Details about user</Link>
            <a href={props.linkGit} target="_blank">GitHub page</a>
        </div>
    )
}

User.propTypes = {
    name: PropTypes.string,
    linkGit: PropTypes.string,
};