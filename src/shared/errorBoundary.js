import React from 'react';

export default class ErrorBoundary extends React.Component {
    state = {
        error: false
    };

    componentDidCatch() {
         this.setState({error: true})
    }

    render() {
          if (this.state.error) {
              return(
                  <div></div>
              )
          } else {
              return this.props.children;
          }
    }
}