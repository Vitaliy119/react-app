import { LIST_USER, CURRENT_USER } from "../actionConst";

const initialState = {
    userList: [],
    currentUser: [],
};

export function user (state = initialState, action) {
    switch(action.type) {
        case LIST_USER:
            return {
                ...state,
                userList: action.payload
            };
            break;
        case CURRENT_USER:
            return {
                ...state,
                currentUser: action.payload
            };
            break;
        default: return state;
    }

}