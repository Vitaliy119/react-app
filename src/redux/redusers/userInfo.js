import { LOGIN_USER } from '../actionConst';

export function userLogin(state = false, action) {
    if (action.type === LOGIN_USER) {
        return action.payload;
    }
    return state;
};
