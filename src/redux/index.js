'use strict';

import { combineReducers } from 'redux';
import { userLogin } from './redusers/userInfo';

export default combineReducers({
    userLogin,
});
