import axios from "axios/index";
import {apiPath} from "./apiConst";

export const userList = (name) => {
   return axios.get(`${apiPath}/search/users?per_page=10&q=${name}`);
};

export const currentUser = (name) => {
  return axios.get(`${apiPath}/users/${name}`);
};